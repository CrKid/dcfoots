
<!doctype html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Авторизуйтесь, пожалуйста</title>
    
    <link rel="apple-touch-icon" sizes="57x57" href="/assets/images/icon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/assets/images/icon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/assets/images/icon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/assets/images/icon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/assets/images/icon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/assets/images/icon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/assets/images/icon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/assets/images/icon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/assets/images/icon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/assets/images/icon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/images/icon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/assets/images/icon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/images/icon/favicon-16x16.png">
    <link rel="manifest" href="/assets/images/icon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/assets/images/icon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css?<?=md5_file(__DIR__.'/assets/css/bootstrap.min.css')?>" />

    <!-- Custom styles for this template -->
    <link href="assets/css/signin.css?<?=md5_file(__DIR__.'/assets/css/signin.css')?>" rel="stylesheet">
    
  </head>

  <body class="text-center">
   	<?php include __DIR__.'/assets/images/svg.php';?>
   	
   	<?php if ( $_GET['action'] == 'signup' ) { ?>
   	
		 <form class="form-signin" method="post">
		  <?php /* <h1 class="h3 mb-3 font-weight-normal"><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#YApps_Logo"></use></svg></h1> */ ?>
		  <?php if ( $POSTRes['status'] == 'error' ) { ?>
		  <div class="alert alert-danger" role="alert"><?=$POSTRes['desc']?></div>
		  <?php } ?>
		  <input type="hidden" name="form" value="signUp" />
		  <input type="text" class="form-control" placeholder="Имя" name="name" required autofocus>
		  <input type="email" class="form-control" placeholder="Email" name="email" required autofocus>
		  <input type="password" class="form-control" placeholder="Пароль" name="passwd" required>
		  <input type="password" class="form-control" placeholder="Подтверждение" name="confimpasswd" required>
		  <button class="btn btn-lg btn-primary btn-block" type="submit">Зарегистрироваться</button>
		  <p class="mt-5 mb-3 text-muted">&copy; <?=date('Y')?>. Юг-Авто.</p>
		</form>
   	
   	<?php } else { ?>
    
		 <form class="form-signin" method="post">
		  <?php /* <h1 class="h3 mb-3 font-weight-normal"><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#YApps_Logo"></use></svg></h1>*/ ?>
		  <?php if ( $POSTRes['status'] == 'error' ) { ?>
		  <div class="alert alert-danger" role="alert"><?=$POSTRes['desc']?></div>
		  <?php } ?>
		  <input type="hidden" name="form" value="signIn" />
		  <input type="email" class="form-control" placeholder="Email" name="email" required autofocus>
		  <input type="password" class="form-control" placeholder="Пароль" name="passwd" required>
		  <button class="btn btn-lg btn-primary btn-block" type="submit">Войти</button>
		  <p class="mt-5 mb-3 text-muted"><a href="/?action=signup">Зарегистрироваться</a></p>
		  <p class="mt-5 mb-3 text-muted">&copy; <?=date('Y')?>. Юг-Авто.</p>
		</form>
    
    <?php } ?>
    
    <script>
	if ('serviceWorker' in navigator) {
	 window.addEventListener('load', function() {  
	   navigator.serviceWorker.register('/sw.js').then(
		 function(registration) {
		   // Registration was successful
		   console.log('ServiceWorker registration successful with scope: ', registration.scope); },
		 function(err) {
		   // registration failed :(
		   console.log('ServiceWorker registration failed: ', err);
		 });
	 });
	}
	</script>
    
  </body>
</html>