<?php 
	$arRes = $app->getData( $app->getAuthUser() );
?>
<!doctype html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Менеджер ДЦ Юг-Авто</title>
    
    <link rel="apple-touch-icon" sizes="57x57" href="/assets/images/icon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/assets/images/icon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/assets/images/icon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/assets/images/icon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/assets/images/icon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/assets/images/icon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/assets/images/icon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/assets/images/icon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/assets/images/icon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/assets/images/icon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/images/icon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/assets/images/icon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/images/icon/favicon-16x16.png">
    <link rel="manifest" href="/assets/images/icon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/assets/images/icon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css?<?=md5_file(__DIR__.'/assets/css/bootstrap.min.css')?>" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    
    <link rel="stylesheet" href="assets/css/app.css?<?=md5_file(__DIR__.'/assets/css/app.css')?>" />

  </head>

  <body>
	<?php include __DIR__.'/assets/images/svg.php';?>
    <header>
      <div class="collapse bg-dark" id="navbarHeader">
        <div class="container">
          <div class="row">
            <div class="col-sm-8 col-md-7 py-4">
              <h4 class="text-white"><?=$arRes['user']['name']?></h4>
              <?php /* <p class="text-muted">№ рабочего листа: <span role="id"></span></p> */ ?>
            </div>
            <div class="col-sm-4 offset-md-1 py-4">
              <h4 class="text-white">Управление</h4>
              <ul class="list-unstyled">
                <li><a href="/today.php" class="text-white"><i class="fa fa-calendar" aria-hidden="true"></i> Сегодня</a></li>
                <li><a href="/" class="text-white"><i class="fa fa-times" aria-hidden="true"></i> Сбросить</a></li>
                <?php if ( $app->getAuthUser()['role']==1 ) { ?>
                <li><a href="/stat.php" class="text-white"><i class="fa fa-calendar" aria-hidden="true"></i> Статистика</a></li>
                <?php } ?>
                <li><a href="/?action=logout" class="text-white"><i class="fa fa-sign-out" aria-hidden="true"></i> Выход</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="navbar navbar-dark bg-dark box-shadow">
        <div class="container d-flex justify-content-between">
          <a href="/" class="navbar-brand d-flex align-items-center"><i class="fa fa-car" aria-hidden="true"></i>&nbsp;&nbsp;<?=$arRes['dcs'][0]['brand']?></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
        </div>
      </div>
    </header>

    <main role="main">
	  
      <?php // $app::sp( $arRes ); ?>
      
      <div class="album py-5 bg-light">
        <div class="container">
		  
          <?php // $app->sp($arRes, true); ?>
          <?php $s = 1; ?>
          <div class="row" role="steps" name="qt" data-step="<?=$s?>" style="display: -webkit-box;display: -ms-flexbox;display: flex;-ms-flex-wrap: wrap;">
          	<h1 class="text-center col-md-12">Количество посетителей в группе</h1>
          	<div class="clear" style="margin-bottom: 50px;"><br /><br /></div>
          	<?php for ($i=1; $i<=8; $i++) { ?>
          	<div class="col-md-6 mb-3">
          		<button type="button" role="next" data-step="<?=$s+1?>" data-id="<?=$i?>" class="btn btn-success btn-lg"><?=$i?></button>
          	</div>
          	<?php } // for ?>
          </div>
          <?php $s++; ?>
          
          
          <?php if ( count($arRes['dcs'])>1 ) { ?>
          <div class="row" role="steps" name="dc_id" data-step="<?=$s?>">
			<h1 class="text-center col-md-12">Дилерский центр</h1>
          	<div class="clear" style="margin-bottom: 50px;"><br /><br /></div>
			<?php foreach ( $arRes['dcs'] as $k => $item ) { ?>
				<div class="col-md-12 text-center" style="margin-bottom: 20px;">
				  <button type="button" role="next" data-step="<?=$s+1?>" data-id="<?=$item['dc_id']?>" class="btn btn-info"><?=$item['brand']?>. <?=$item['name']?></button>
				</div>
			<?php } // foreach ?>
		  </div>
          <?php $s++; ?>
          <?php } ?>
          
          <div class="row" role="steps" name="target_id" data-step="<?=$s?>">
			<h1 class="text-center col-md-12">Цель визита</h1>
          	<div class="clear" style="margin-bottom: 50px;"><br /><br /></div>
			<?php foreach ( $arRes['targets'] as $k => $item ) { ?>
              <?php if ( $k<2 ) { ?>
                <div class="col-md-6 mb-3 text-center">
				  <button type="button" role="next" data-step="<?=$s+1+$item['step']?>" data-addstep="<?=$item['step']?>" data-id="<?=$item['id']?>" class="btn btn-success btn-lg"><?=$item['name']?></button>
				</div>
              <?php } elseif ( $k>=2 && $k<5 ) { ?>
              <div class="col-md-4 mb-3 text-center">
				  <button type="button" role="next" data-step="<?=$s+1+$item['step']?>" data-addstep="<?=$item['step']?>" data-id="<?=$item['id']?>" class="btn btn-success btn-lg"><?=$item['name']?></button>
				</div>
              <?php } else { ?>
				<div class="col-md-12 mb-3 text-center">
				  <button type="button" role="next" data-step="<?=$s+1+$item['step']?>" data-addstep="<?=$item['step']?>" data-id="<?=$item['id']?>" class="btn btn-<?=(($k<=1)?'success':'info')?> <?=(($k<=1)?'btn-lg':'')?>"><?=$item['name']?></button>
				</div>
              <?php } ?>
			  <?php if ( $k ==1 || $k ==4 ) { ?>
                  <div class="clear" style="height: 100px;"></div>
              <?php } // if ?>
			<?php } // foreach ?>
		  </div>
          <?php $s++; ?>
         
         
         <?php foreach ( $arRes['dcs'] as $item ) { ?>
         <?php $mRes = $app->getManagersResults( $item['dc_id'] ); ?>
         <div class="row" role="steps" name="manager_id" data-dc="<?=$item['dc_id']?>" data-step="<?=$s?>">
			<h1 class="text-center col-md-12">Менеджер</h1>
          	<div class="clear" style="margin-bottom: 50px;"><br /><br /></div>
			<?php foreach ( $item['managers'] as $k => $m ) { ?>
				<div class="col-md-8 text-right" style="margin-bottom: 20px;">
				  <button type="button" role="next" data-step="<?=$s+1?>" data-id="<?=$m['id']?>" class="btn btn-info w-100"><?=$m['name']?></button>
                  <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: <?=$mRes[$k]/max($mRes)*100?>%;" aria-valuemin="0" aria-valuemax="<?=max($mRes)?>"></div>
                  </div>
				</div>
				<div class="col-md-4 text-left" role="check">
					<svg class="YApps-Icon" xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#YApps-UnCheck"></use></svg> <span>Предв. согл.</span>
				</div>
			<?php } // foreach ?>
		  </div>
          <?php } // foreach ?>
          <?php $s++; ?>
         
          <div class="row" role="steps" data-step="<?=$s?>">
			<h1 class="text-center col-md-12">Завершение</h1>
         	<?php /* <h3 class="text-center col-md-12">№ рабочего листа: <span role="id"></span></h3> */ ?>
          	<div class="clear" style="margin-bottom: 50px;"><br /><br /></div>
          	
          	<div class="col-md-1"></div>
          	<div class="col-md-10 text-center" style="margin-bottom: 20px;">
          		Комментарий:
          		<textarea class="form-control" rows="6" name="comment"></textarea>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-12 text-center" style="margin-bottom: 20px;">
			  <button type="button" role="finish" class="btn btn-success btn-lg" style="width: 83%">Завершить</button>
			</div>
			<div class="col-md-12 text-center" role="spiner"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>
			<div class="col-md-12 text-center" role="error">
				<div class="alert alert-dismissible alert-danger">
				  <button type="button" class="close" data-dismiss="alert">&times;</button>
				  <strong>Ой!</strong> Что-то пошло не так.
				</div>
			</div>
		  </div>
          
        </div>
      </div>

    </main>

    <footer class="text-muted">
      <div class="container">
        <p class="text-center" style="margin-top: 15px;">&copy; <?=date('Y')?>. Юг-Авто.</p>
      </div>
    </footer>
	
    <div id="form">
    	<input type="hidden" name="user_id" value="<?=$arRes['user']['id']?>" />
    	<input type="hidden" name="qt" value="" />
    	<input type="hidden" name="dc_id" value="<?=((count($arRes['dcs'])==1)?$arRes['dcs'][0]['dc_id']:'')?>" />
    	<input type="hidden" name="target_id" value="" />
    	<input type="hidden" name="manager_id" value="" />
    </div>
   
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/holder/2.9.4/holder.min.js"></script>
    
    <script src="assets/js/app.js?<?=md5_file(__DIR__.'/assets/js/app.js')?>"></script>
  </body>
</html>
