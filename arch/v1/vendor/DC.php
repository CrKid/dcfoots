<?php
	class DC {
		
		/* Init */
		public function __construct( $arConf = [] ) {
			
			$this->db			= new SafeMySQL( $arConf['db'] );
			$this->Mailer 		= new PHPMailer();
		} 
		
		public static function sp( $q, $v = false ) { echo '<pre '.(($v)?'style="display: none;"':'').'>'; print_r( $q ); echo '</pre>'; }
		
		public function signIn( $POST ) {
			
			$user = $this->db->getRow('SELECT * FROM users WHERE email = ?s', $POST['email']);
			
			if ( password_verify($POST['passwd'], $user['passwd']) ) {
				
				return $_SESSION['USER'] = $user;
				
			} else {
				
				return ['status'=>'error', 'desc'=>'Пароль не подходит'];
			}
		}
		
		public function signUp( $POST ) {
			
			if ( $POST['passwd'] != $POST['confimpasswd'] ) {
				
				return ['status'=>'error', 'desc'=>'Пароли не совпадают'];
				
			} else {
				
				$arIns['name'] = $POST['name'];
				$arIns['email'] = $POST['email'];
				$arIns['passwd'] = password_hash($POST['passwd'], PASSWORD_DEFAULT);
				
				$this->db->query('INSERT INTO users SET ?u', $arIns);
				
				$p = [
					'email' => $POST['email'],
					'passwd' => $POST['passwd'],
					'form' => 'signIn'
				];
				
				$this->signIn( $p );
				
				return ['status'=>'success'];
			}
		}
		
		public function singOut() {
			
			return session_destroy();
		}
		
		public function isAutorized() {
			
			return ($_SESSION['USER']) ? true : false;
		}
		
		public function getAuthUser() {
			
			return $_SESSION['USER'];
		}
		
		public static function redirect($route) {
			
			header('Location: '.$route);
		}
		
		public function getData( $user ) {
			
			$res['user'] = $user;
			$res['targets'] = $this->db->getAll('SELECT * FROM targets ORDER BY sort ASC');
			$query =
				'SELECT * 
				FROM users_dcs 
				INNER JOIN dcs
				ON users_dcs.dc_id = dcs.id 
				WHERE user_id = ?i';
			$res['dcs'] = $this->db->getAll($query, $res['user']['id']);
			foreach ( $res['dcs'] as $k => $v ) {
				
				$query = 
					'SELECT * 
					FROM managers_dcs 
					INNER JOIN managers
					ON managers_dcs.manager_id = managers.id 
					WHERE dc_id = ?i';
				//$res['dcs'][$k]['managers'] = $this->db->getAll($query, $v['dc_id']);
				$res['dcs'][$k]['managers'] = $this->getManagers($v['id']);
			} // foreach
			$res['sources'] = $this->db->getAll('SELECT * FROM sources');
			
			return $res;
		}
		
		public function getManagers( $dc ) {
			
			$sch = $this->db->getCol('SELECT id FROM schedules WHERE start < CAST(CURTIME() AS time) AND end > CAST(CURTIME() AS time)');
			$d_ids = $this->db->getCol('SELECT manager_id FROM managers_dcs WHERE dc_id = ?i', (int)$dc);
			$ids = $this->db->getCol('SELECT manager_id FROM managers_schedules WHERE manager_id IN (?a) AND schedule_id IN (?a) AND date = CAST(CURDATE() AS date)', $d_ids, $sch);
			
			return $this->db->getAll('SELECT * FROM managers WHERE id IN (?a)', $ids);
		}
		
		public function getManagersIDs( $dc ) {
			
			$sch = $this->db->getCol('SELECT id FROM schedules WHERE start < CAST(CURTIME() AS time) AND end > CAST(CURTIME() AS time)');
			$d_ids = $this->db->getCol('SELECT manager_id FROM managers_dcs WHERE dc_id = ?i', (int)$dc);
			$ids = $this->db->getCol('SELECT manager_id FROM managers_schedules WHERE manager_id IN (?a) AND schedule_id IN (?a) AND date = CAST(CURDATE() AS date)', $d_ids, $sch);
			
			return $this->db->getCol('SELECT id FROM managers WHERE id IN (?a)', $ids);
		}
		
		public function getManagersResults( $dc ) {
			
			foreach ( $this->getManagersIDs($dc) as $id ) $res[] = $this->db->getOne('SELECT COUNT(*) FROM stat WHERE manager_id = ?i', $id);
			return $res;
		}
		
		public function sendRes( $POST ) {
			
			$arIns['user_id'] = (int)$POST['user_id'];
			$arIns['manager_id'] = (int)$POST['manager_id'];
			$arIns['dc_id'] = (int)$POST['dc_id'];
			$arIns['target_id'] = (int)$POST['target_id'];
			$arIns['qt'] = (int)$POST['qt'];
			$arIns['accord_flag'] = (int)$POST['accord_flag'];
			$arIns['source_id'] = (int)$POST['source_id'];
			$arIns['comment'] = $POST['comment'];
			$arIns['timestamp'] = time();
			
			return $this->db->query('INSERT INTO results SET ?u', $arIns);
		}
		
		public function setStat( $POST ) {
			
			$arIns['user_id'] = (int)$POST['user_id'];
			$arIns['manager_id'] = (int)$POST['manager_id'];
			$arIns['dc_id'] = (int)$POST['dc_id'];
			$arIns['target_id'] = (int)$POST['target_id'];
			$arIns['quantity'] = (int)$POST['qt'];
			$arIns['approval'] = (int)$POST['accord_flag'];
			$arIns['source_id'] = (int)$POST['source_id'];
			$arIns['comment'] = $POST['comment'];
			$arIns['timestamp'] = time();
			
			return $this->db->query('INSERT INTO stat SET ?u', $arIns);
		}
		
		public function setWorkList( $POST ) {
			
			return $this->db->query('UPDATE stat SET ?u WHERE id = ?i', ['work_list' => $POST['wl']], $POST['id']);	
		}
		
		public function getToday( $user ) {
			
			if ( $user['role'] == 1 ) {
				
				$res = $this->db->getAll('SELECT * FROM stat WHERE timestamp > ?i', strtotime(date('Y-m-d')));
				
			} else {
				
				$res = $this->db->getAll('SELECT * FROM stat WHERE user_id = ?i AND timestamp > ?i', $user['id'], strtotime(date('Y-m-d')));
			}
			
			foreach ( $res as $k => $r ) {
				
				$res[$k]['manager'] = $this->db->getRow('SELECT * FROM managers WHERE id = ?i', $r['manager_id']);
				$res[$k]['dc'] = $this->db->getRow('SELECT * FROM dcs WHERE id = ?i', $r['dc_id']);
				$res[$k]['target'] = $this->db->getRow('SELECT * FROM targets WHERE id = ?i', $r['target_id']);
				$res[$k]['user'] = $this->db->getRow('SELECT * FROM users WHERE id = ?i', $r['user_id']);
			}
			
			
			return $res;
		}
		
		public function getDCs() {
			
			return $this->db->getAll('SELECT * FROM dcs');
		}
		
		public function getAll( $date1 = false, $date2 = false, $dc = false ) {
			
			$query = 'SELECT * FROM stat';
			if ( $date1 ) $query .= ' WHERE timestamp >= '.strtotime($date1).' AND timestamp <= '.(($date2)?strtotime($date2):time());
			if ( $dc ) $query .= ' AND dc_id = '.$dc;
			$res = $this->db->getAll( $query );
			
			foreach ( $res as $k => $r ) {
				
				$res[$k]['manager'] = $this->db->getRow('SELECT * FROM managers WHERE id = ?i', $r['manager_id']);
				$res[$k]['dc'] = $this->db->getRow('SELECT * FROM dcs WHERE id = ?i', $r['dc_id']);
				$res[$k]['target'] = $this->db->getRow('SELECT * FROM targets WHERE id = ?i', $r['target_id']);
				$res[$k]['user'] = $this->db->getRow('SELECT * FROM users WHERE id = ?i', $r['user_id']);
				
				( $result['targets'][$res[$k]['target']['id']] ) ? $result['targets'][$res[$k]['target']['id']]['count']++ : $result['targets'][$res[$k]['target']['id']] = ['name'=>$res[$k]['target']['name'], 'count'=>1];
			}
			
			$result['items'] = $res;
			
			return $result;
		}
		
		public function getUserStat( $date1 = false, $date2 = false, $user ) {
			
			$query = 'SELECT * FROM stat';
			if ( $date1 ) $query .= ' WHERE user_id = '.$user['id'].' AND timestamp >= '.strtotime($date1).' AND timestamp <= '.(($date2)?strtotime($date2):time());
			if ( $dc ) $query .= ' AND dc_id = '.$dc;
			$res = $this->db->getAll( $query );
			
			foreach ( $res as $k => $r ) {
				
				$res[$k]['manager'] = $this->db->getRow('SELECT * FROM managers WHERE id = ?i', $r['manager_id']);
				$res[$k]['dc'] = $this->db->getRow('SELECT * FROM dcs WHERE id = ?i', $r['dc_id']);
				$res[$k]['target'] = $this->db->getRow('SELECT * FROM targets WHERE id = ?i', $r['target_id']);
				$res[$k]['user'] = $this->db->getRow('SELECT * FROM users WHERE id = ?i', $r['user_id']);
				
				( $result['targets'][$res[$k]['target']['id']] ) ? $result['targets'][$res[$k]['target']['id']]['count']++ : $result['targets'][$res[$k]['target']['id']] = ['name'=>$res[$k]['target']['name'], 'count'=>1];
			}
			
			$result['items'] = $res;
			
			return $result;
		}
	}