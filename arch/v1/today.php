<?php
	/*
	ini_set('error_reporting', E_ALL);
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	*/
	require_once __DIR__.'/vendor/Config.php';
	require_once __DIR__.'/vendor/SafeMySQL.php';
	require_once __DIR__.'/vendor/PHPMailer.php';
	require_once __DIR__.'/vendor/DC.php';

	session_start();

	$IP = $_SERVER['HTTP_X_REAL_IP'];
	$app = new DC( $arConf );

	if ( $app->isAutorized() ) {
		 ?>
<?php $arRes = $app->getToday( $app->getAuthUser() );?>
<!doctype html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Менеджер ДЦ Юг-Авто</title>
    
    <link rel="apple-touch-icon" sizes="57x57" href="/assets/images/icon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/assets/images/icon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/assets/images/icon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/assets/images/icon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/assets/images/icon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/assets/images/icon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/assets/images/icon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/assets/images/icon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/assets/images/icon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/assets/images/icon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/images/icon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/assets/images/icon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/images/icon/favicon-16x16.png">
    <link rel="manifest" href="/assets/images/icon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/assets/images/icon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css?<?=md5_file(__DIR__.'/assets/css/bootstrap.min.css')?>" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/css/jquery.dataTables.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/remodal/1.1.1/remodal.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/remodal/1.1.1/remodal-default-theme.min.css" />

    <link rel="stylesheet" href="assets/css/app.css?<?=md5_file(__DIR__.'/assets/css/app.css')?>" />

  </head>

  <body>
	<?php include __DIR__.'/assets/images/svg.php';?>
    <header>
      <div class="collapse bg-dark" id="navbarHeader">
        <div class="container">
          <div class="row">
            <div class="col-sm-8 col-md-7 py-4">
              <h4 class="text-white"><?=$app->getAuthUser()['name']?></h4>
              <p class="text-muted">№ рабочего листа: <span role="id"></span></p>
            </div>
            <div class="col-sm-4 offset-md-1 py-4">
              <h4 class="text-white">Управление</h4>
              <ul class="list-unstyled">
                <li><a href="/today.php" class="text-white"><i class="fa fa-calendar" aria-hidden="true"></i> Сегодня</a></li>
                <li><a href="/" class="text-white"><i class="fa fa-times" aria-hidden="true"></i> Сбросить</a></li>
                <?php if ( $app->getAuthUser()['role']==1 ) { ?>
                <li><a href="/stat.php" class="text-white"><i class="fa fa-calendar" aria-hidden="true"></i> Статистика</a></li>
                <?php } ?>
                <li><a href="/?action=logout" class="text-white"><i class="fa fa-sign-out" aria-hidden="true"></i> Выход</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="navbar navbar-dark bg-dark box-shadow">
        <div class="container d-flex justify-content-between">
          <a href="/" class="navbar-brand d-flex align-items-center"><i class="fa fa-car" aria-hidden="true"></i>&nbsp;&nbsp;<?=$arRes['dcs'][0]['brand']?></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
        </div>
      </div>
    </header>

    <main role="main">

      <div class="album py-5 bg-light">
        <div class="container">

			<h1>Посетители <?=date('d/m/Y')?></h1>

			<table class="table table-striped table-hover table-sm dataTable" id="data-table">
			  <thead>
				<tr>
				  <th scope="col" style="width: 5%">ID</th>
				  <th scope="col" style="width: 10%">ДЦ</th>
				  <th scope="col" style="width: 10%">Хостес</th>
				  <th scope="col" style="width: 15%">Менеджер</th>
                  <th scope="col" style="width: 5%">Предв.</th>
                  <th scope="col" style="width: 10%">Раб. лист</th>
				  <th scope="col" style="width: 15%">Цель</th>
				  <th scope="col" style="width: 20%">Комментарий</th>
				  <th scope="col" style="width: 10%">Время</th>
				</tr>
			  </thead>
			  <tbody>
			  	<?php  foreach( $arRes as $item ) { ?>
				<tr data-id="<?=$item['id']?>">
					<td><?=$item['id']?></td>
					<td><?=$item['dc']['name']?></td>
					<td><?=$item['user']['name']?></td>
					<td><?=$item['manager']['name']?></td>
                    <td><?=(($item['approval']==1)?'<i class="fa fa-check-square-o" aria-hidden="true"></i>':'')?></td>
                    <td role="wl"><?=(($item['work_list'])?:'<a href="#modal" data-id="'.$item['id'].'" role="wl"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>')?></td>
					<td><?=$item['target']['name']?></td>
					<td><?=$item['comment']?></td>
					<td><?=date('H:i', $item['timestamp'])?></td>
				</tr>
				<?php } ?>
			  </tbody>

			</table>
		</div>
	  </div>
	</main>

	</main>
    
    <div class="remodal" data-remodal-id="modal">
      <button data-remodal-action="close" class="remodal-close"></button>
      <h1>№ Рабочего листа</h1>
      <p>
        <input type="text" name="wl" />
        <input type="hidden" name="id" />
      </p>
      <br>
      <button data-remodal-action="confirm" class="remodal-confirm">OK</button>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/holder/2.9.4/holder.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/remodal/1.1.1/remodal.min.js"></script>

    <script src="assets/js/app.js?<?=md5_file(__DIR__.'/assets/js/app.js')?>"></script>
		<script>
		$("#data-table").DataTable({
			"order": [[ 0, "desc" ]]
		});
		</script>
  </body>
</html>

	<?php }
?>
