<?php
	/*
	ini_set('error_reporting', E_ALL);
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	*/
	require_once __DIR__.'/vendor/Config.php';
	require_once __DIR__.'/vendor/SafeMySQL.php';
	require_once __DIR__.'/vendor/PHPMailer.php';
	require_once __DIR__.'/vendor/DC.php';

	session_start();

	$IP = $_SERVER['HTTP_X_REAL_IP'];
	$app = new DC( $arConf );
	
	$date1 = ( $_GET['date1'] ) ?: date('Y-m-d H:i', time()-24*60*60);
	$date2 = ( $_GET['date2'] ) ?: date('Y-m-d H:i');
	$dc = ( (int)$_GET['dc'] ) ?: 1;
	
	//print_r($_GET); die;

	if ( $app->isAutorized() ) {
		
		$arRes = $app->getAll( $date1, $date2, $dc );
		
		$headers = [
			iconv('UTF-8', 'WINDOWS-1251', 'ID записи'),
			iconv('UTF-8', 'WINDOWS-1251', 'ДЦ'),
			iconv('UTF-8', 'WINDOWS-1251', 'Хостес'),
			iconv('UTF-8', 'WINDOWS-1251', 'Менеджер'),
			iconv('UTF-8', 'WINDOWS-1251', 'Предв.'),
			iconv('UTF-8', 'WINDOWS-1251', 'Цель'),
			iconv('UTF-8', 'WINDOWS-1251', 'Комментарий'),
			iconv('UTF-8', 'WINDOWS-1251', 'Дата')
		];
		
		
		$now = gmdate("D, d M Y H:i:s");
		$filename = date('Y-m-d_H-i').'.csv';
		header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
		header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
		header("Last-Modified: {$now} GMT");
	
		// force download
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
	
		// disposition / encoding on response body
		header("Content-Disposition: attachment;filename={$filename}");
		header("Content-Transfer-Encoding: binary");

		$handle = fopen("php://output", 'w');
		fputcsv( $handle, $headers, ';' );
		
		foreach ( $arRes['items'] as $r ) {
			
			$line[] = $r['id'];
			$line[] = iconv('UTF-8', 'WINDOWS-1251', $r['dc']['name']);
			$line[] = iconv('UTF-8', 'WINDOWS-1251', $r['user']['name']);
			$line[] = iconv('UTF-8', 'WINDOWS-1251', $r['manager']['name']);
			$line[] = ( $r['accord_flag'] == 1 ) ? iconv('UTF-8', 'WINDOWS-1251', 'Да') : '';
			$line[] = iconv('UTF-8', 'WINDOWS-1251', $r['target']['name']);
			$line[] = iconv('UTF-8', 'WINDOWS-1251', $r['comment']);
			$line[] = date('Y-m-d H:i', $r['timestamp']);
			
			fputcsv( $handle, $line, ';' );
			
			unset( $line );
		}

		fclose($handle);
	
	} else {
		
		$app->sp( 'Извините, вам необходимо авторизоваться.' );
	}
?>