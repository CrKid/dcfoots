<?php
	/*
	ini_set('error_reporting', E_ALL);
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	*/
	require_once __DIR__.'/vendor/Config.php';
	require_once __DIR__.'/vendor/SafeMySQL.php';
	require_once __DIR__.'/vendor/PHPMailer.php';
	require_once __DIR__.'/vendor/DC.php';
	
	session_start();

	$IP = $_SERVER['HTTP_X_REAL_IP'];

	if ( !in_array( $IP, $arConf['ips'] ) ) {
		
		$app = new DC( $arConf );
		if ( $_POST ) include __DIR__.'/vendor/_POST.php';
		if ( $_GET ) include __DIR__.'/vendor/_GET.php';
		
		include ( $app->isAutorized() ) ? 'content.php' : 'login.php';
	
	} // if