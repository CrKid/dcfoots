'use strict';
/*

var CID = $.ajax({type: "GET", url: "ajax/max.php", async: false}).responseText;
$('span[role="id"]').text( CID )

setInterval( function() {
	
	var CID = $.ajax({type: "GET", url: "ajax/max.php", async: false}).responseText;
	$('span[role="id"]').text( CID )
	
}, 10000);
*/

$('button[role="next"]').click( function() {
	
	var $container = $(this).parent().parent();
	var step = Number( $(this).data('step') );
	var val = $(this).data('id');
	var dc = $('input[name="dc_id"]').val();
	
	$('input[name="'+$($container).attr('name')+'"]').val( val );
	
	$($container).hide();
	
	if ( $($container).attr('name') == 'target_id' && $(this).data('addstep') == '0' ) {
		
		$('div[data-step="'+step+'"][data-dc="'+dc+'"]').css({'display':'-webkit-box', 'display':'-ms-flexbox', 'display':'flex', '-ms-flex-wrap':'wrap'});
		
	} else {
		
		$('div[data-step="'+step+'"]').css({'display':'-webkit-box', 'display':'-ms-flexbox', 'display':'flex', '-ms-flex-wrap':'wrap'});
	}
});

$('div[role="check"]').click( function() {
	
	var icon = $(this).children('svg').children('use').attr('xlink:href');
	
	if ( icon == '#YApps-UnCheck' ) $(this).children('svg').children('use').attr('xlink:href', '#YApps-Check');
	if ( icon == '#YApps-Check' ) $(this).children('svg').children('use').attr('xlink:href', '#YApps-UnCheck');
});

$('button[role="finish"]').click( function() {
	
	$('div[role="spiner"]').show();
	
	var send = {};
	
	send.user_id = $('input[name="user_id"]').val();
	send.manager_id = $('input[name="manager_id"]').val();
	send.dc_id = $('input[name="dc_id"]').val();
	send.target_id = $('input[name="target_id"]').val();
	send.qt = $('input[name="qt"]').val();
	send.accord_flag = 0;
	send.action = 'res';
	
	$('div[role="check"]').each( function(i,e) {
		
		if ( $(this).children('svg').children('use').attr('xlink:href') == '#YApps-Check' ) send.accord_flag = 1;
	});
	
	send.source_id = $('select[name="source_id"]').val();
	send.comment = $('textarea[name="comment"]').val();
	
	console.log( send );
	
	$.ajax({
		type: "POST",
		url: 'ajax/',  
		data: send,
		success: function() {
			
			location.reload();
		},
		error: function() {
			
			$('div[role="error"]').show()
		}
	});
});

$('a[role="wl"]').click( function() {
	
	$('input[name="id"]').val( $(this).data('id') );
});

$('button.remodal-confirm').click( function() {
	
	var send = {};
	
	send.id = $('input[name="id"]').val();
	send.action = 'wl';
	send.wl = $('input[name="wl"]').val();
	
	$.ajax({
		type: "POST",
		url: 'ajax/',  
		data: send,
		success: function() {
			
			location.reload();
		},
		error: function() {
			
			$('div[role="error"]').show()
		}
	});
});