<?php
	/*
	ini_set('error_reporting', E_ALL);
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	*/
	require_once __DIR__.'/../vendor/Config.php';
	require_once __DIR__.'/../vendor/SafeMySQL.php';
	require_once __DIR__.'/../vendor/PHPMailer.php';
	require_once __DIR__.'/../vendor/DC.php';

	if ( $_POST ) {
		
		$app = new DC( $arConf );
		
		switch ( $_POST['action'] ) {
			
			case 'res': $app->sendRes( $_POST ); $app->setStat( $_POST ); break;
			case 'wl': $app->setWorkList( $_POST ); break;
		}
	}