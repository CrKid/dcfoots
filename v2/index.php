<!doctype html>
<html lang="ru">
  <head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Anton V Boretscy">
    <title>Фиксация трафика в ДЦ Юг-Авто</title>

	<link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha256-eZrrJcwDc/3uDhsdt61sL2oOBY362qM3lon1gyExkL0=" crossorigin="anonymous" />
    <link href="assets/css/app.css?<?=md5_file(__DIR__.'/assets/css/app.css');?>" rel="stylesheet">
    
  </head>
  <body class="pb-5"><div id="app">
    
    <div class="bg-yadarkblue c-yawhite py-2 mb-3">
      <div class="container">
        <div class="row top-row">
          <div class="col-3">
            <a href="/v2/">
              <svg xmlns="http://www.w3.org/2000/svg">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Logo-YA-M"></use>
              </svg>
            </a>
          </div>
          <div class="col pt-2 text-center" v-if="auth.token">
            <a href="#" v-if="step == 1" @click.prevent="NextStep({}, 100)"><i class="fa fa-plus"></i> Добавить рабочий лист</a>
            <a href="#" v-if="step != 1" @click.prevent="Reset()"><i class="fa fa-refresh" aria-hidden="true"></i> Отменить</a>
          </div>
          <div class="col-3 pt-2 text-right">
            <a href="#" @click.prevent="UnAuth()" v-if="auth.token">Выйти <i class="fa fa-sign-out"></i></a>
          </div>
        </div>
      </div>
    </div>
    
    <div class="container" v-if="!auth.token">
      <div class="row">
        <div class="col-md-12 text-center py-3"><h3>Авторизация</h3></div>
        <div class="col-md-12">
          <form autocomplete="off">
            <div class="form-group">
              <input type="email" class="form-control form-control-lg" name="email" placeholder="Email" v-model="email">
            </div>
            <div class="form-group">
              <input type="password" class="form-control form-control-lg" name="password" placeholder="Пароль *" v-model="passwd">
            </div>
            <div class="form-group"><a class="but but-blue" role="butLogin" href="#" @click.prevent="Auth({email: email, passwd: passwd})">Войти</a></div>
          </form>
          <div class="col-12 py-2 px-0 mb-2" v-if="auth.status == 'error'">
            <div class="alert alert-danger">{{ auth.description }}</div>
          </div>
        </div>
      </div>
    </div>
    
    <div class="container" v-if="auth.token && step == 1">
      <div class="row">
        <div class="col-md-12 text-center py-3"><h3>Количество посетителей</h3></div>
      </div>
      <div class="row text-center">
        <div class="col-6 mb-3 d-flex justify-content-center" v-for="n in 8"><div class="but but-blue but-circle circle-ladge" @click.prevent="NextStep({quantity: n}, 2)"><span class="h1">{{ n }}</span></div></div>
      </div>
    </div>
    
    <div class="container" v-if="auth.token && step == 2">
      <div class="row">
        <div class="col text-center py-3"><h3>Цель визита</h3><a href="#" @click.prevent="NextStep({}, 1)"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> назад</a></div>
      </div>
      <div class="row text-center mb-4" v-for="(target, key) in targets">
        <div class="col-12 mb-2 " v-for="targ in target">
          <a class="h4 m-0 but d-block" 
          	v-bind:class="{ 'py-4 but-blue': key==1, 'py-3 but-blue': key==2, 'py-2 but-gray': key==3 }" 
            @click.prevent="NextStep({target_id: targ.id}, targ.next_step)"
            >{{ targ.ru_name }}</a>
        </div>
      </div>
    </div>
    
    <div class="container" v-if="auth.token && step == 3">
      <div class="row">
        <div class="col-md-12 text-center py-3"><h3>Менеджер</h3><a href="#" @click.prevent="NextStep({}, 2)"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> назад</a></div>
      </div>
      <div class="row text-center">
        <div class="col-10 py-2"></div>
        <div class="col-1 py-2"></div>
        <div class="col-1 py-2"><small>Предв. согл.</small></div>
      </div>
      
      <div class="row mb-2 text-center" v-for="manager in managers" :key="manager.id">
        <div class="col-1 h4 m-0 but" v-bind:class="[Number(manager.results.current) > 0 ? 'but-blue' : 'but-gray']">{{ manager.results.current }}</div>
        <div class="col-1 py-2"></div>
        <div class="col-8 p-0">
          <a class="py-2 but but-gray h4 m-0 d-block" @click.prevent="NextStep({manager_id: manager.id, arrangement: manager.arrangement}, 4)">{{ manager.ru_name }}</a>
        </div>
        <div class="col-1 py-2"></div>
        <div class="col-1 p-0">
          <a class="d-block py-2 m-0 h4 but" @click.prevent="manager.arrangement = !manager.arrangement" v-bind:class="[manager.arrangement ? 'but-blue' : 'but-gray']">
              <i class="fa" aria-hidden="true"
                v-bind:class="[manager.arrangement ? 'fa-check-square-o' : 'fa-square-o']"
                >
              </i>
          </a>
        </div>
        <div class="col-2 py-2"></div>
        <div class="col-8 p-0 mb-2">
          <div class="progress bg-yawhite" style="height: 3px;">
            <div class="progress-bar bg-yayellow" role="progressbar" v-bind:style="{ 'width': manager.results.percent }" v-bind:aria-valuenow="manager.results.percent_raw" aria-valuemin="0" aria-valuemax="100"></div>
          </div>
        </div>
        <div class="col-1 py-2"></div>
        
      </div>
    </div>
    
    <div class="container" v-if="auth.token && step == 4">
      <div class="row">
        <div class="col-md-12 text-center py-3"><h3>Контакты</h3><a href="#" @click.prevent="NextStep({}, 3)"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> назад</a></div>
      </div>
      <div class="row text-center" id="contact">
        <div class="col-12 p-0 mb-2">
          <div class="form-group">
            <input type="text" class="form-control form-control-lg" placeholder="+7 (___) ___-__-__" v-mask="mask" v-model="phone">
          </div>
          <div class="form-group">
            <input type="text" class="form-control form-control-lg" placeholder="Имя" v-model="name">
          </div>
        </div>
        <div class="col-12 py-2 px-0 mb-2" v-if="FormatPhoneIn(phone).length > 7">
          <div class="alert" 
            v-bind:class="[client.result.status == 'success' ? 'alert-success' : 'alert-secondary']" 
            @click.prevent="phone = FormatPhoneOut( client.result.phone ); name = client.result.name;"
            >{{ client.result.name }}</div>
        </div>
        <div class="col-12 py-2 mb-2 but but-blue" v-if="FormatPhoneIn(phone).length == 11" @click.prevent="NextStep({phone: phone, name: name, agreement: true}, 5)"><div class="h4 m-0">Далее</div></div>
        <div class="col-12 py-2 but but-gray" @click.prevent="NextStep({agreement: false}, 5)"><div class="h4 m-0">Пропустить</div></div>
      </div>
    </div>
    
    <div class="container"  v-if="auth.token && step == 5">
      <div class="row">
        <div class="col-md-12 text-center py-3"><h3>Комментарий</h3><a href="#" @click.prevent="NextStep({}, 4)"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> назад</a></div>
      </div>
      <div class="row text-center">
        <div class="col-12 py-2 mb-2">
          <div class="form-group">
            <textarea class="form-control" id="exampleTextarea" rows="5" v-model="comment"></textarea>
          </div>
        </div>
        <div class="col-12 py-2 mb-2 but but-blue" @click.prevent="SendStat()"><div class="h4 m-0">Отправить</div></div>
        <div class="col-12 py-2 mb-2 but but-gray" v-if="status" @click.prevent="Reset()"><div class="h4 m-0">Завершить</div></div>
      </div>
    </div>
    
    <div class="container"  v-if="auth.token && step == 100">
      <div class="row">
        <div class="col-md-12 text-center py-3"><h3>Посетители сегодня</h3><a href="#" @click.prevent="Reset()"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> назад</a></div>
      </div>
      <div class="row">
        <div class="col-md-12 table-responsive">
          <table class="table table-sm table-hover table-hover">
            <thead>
              <tr>
                <th style="width: 30%">Менеджер</th>
                <th style="width: 20%">Цель визита</th>
                <th style="width: 20%">Рабочий лист</th>
                <th style="width: 15%">Предв. согл</th>
                <th style="width: 15%">Время</th>
              </tr>
            </thead>
            <tbody v-for="stat in stats">
              <tr @click.prevent="show_stat = stat.id">
                <td>{{ stat.manager_name }}</td>
                <td>{{ stat.target_name }}</td>
                <td>{{ stat.work_list }}</td>
                <td class="text-center"><i class="fa fa-check-square-o" aria-hidden="true" v-if="stat.arrangement == '1'"></i></td>
                <td>{{ stat.time }}</td>
              </tr>
              <tr  class="table-secondary" v-if="show_stat == stat.id">
                <td class="pt-2" colspan="3">
                  <div class="form-group">
                    <input type="text" class="form-control" placeholder="Рабочий лист" v-model="stat.work_list">
                    <small class="form-text text-muted">Назначить или обновить рабочий лист</small>
                  </div>
                </td>
                <td class="pt-3"><a href="#" class="but but-blue" @click.prevent="SetWorklist({id: stat.id, work_list: stat.work_list})">Сохранить</a></td>
                <td class="pt-3"><a href="#" class="but but-gray" @click.prevent="show_stat = false">Отменить</a></td>
              </tr>
              <tr  class="table-secondary" v-if="show_stat == stat.id">
                <td>Имя: <br />{{ stat.name }}<small v-if="!stat.name">Неизвестно</small></td>
                <td>Телефон: <br />{{ stat.phone }}<small v-if="!stat.phone">Неизвестно</small></td>
              	<td colspan="3">Комментарий: <br />{{ stat.comment }}</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    
    <div class="bg-yagray fixed-bottom py-2">
      <div class="container">
        <div class="row">
          <div class="col text-center" v-if="auth.token">
            <small><i class="fa fa-map-marker" aria-hidden="true"></i> {{ auth.dc }}</small>
          </div>
          <div class="col text-center" v-if="auth.token">
            <small><i class="fa fa-user-circle-o" aria-hidden="true"></i> {{ auth.name }}</small>
          </div>
        </div>
      </div>
    </div>
    
    <?php include __DIR__.'/include/_svg.php'; ?>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.10/vue.min.js" integrity="sha256-chlNFSVx3TdcQ2Xlw7SvnbLAavAQLO0Y/LBiWX04viY=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.min.js" integrity="sha256-S1J4GVHHDMiirir9qsXWc8ZWw74PHHafpsHp5PXtjTs=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/v-mask/1.3.3/v-mask.min.js" integrity="sha256-DGX5AkANO+skx+hr4txOqTNBjMUoliR8ieSruNiK8wY=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue-cookies@1.5.13/vue-cookies.min.js"></script>
    
    <script src="assets/js/app.js?<?=md5_file(__DIR__.'/assets/js/app.js');?>"></script>
    
  </div></body>
</html>
