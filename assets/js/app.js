'use strict';


var data = function () {
	return {
		auth: {
			status: null,
			description: null,
			token: false,
			dc: null,
			name: null
		},
		
		email: null,
		passwd: null,
		
		mask: '+7 (###) ###-##-##',
		phone: null,
		name: null,
		worklist : null,
		
		comment: null,
		
		client: {
			status: false,
			result: null,
			phone: ''
		},
		
		step: 1,
		send: {},
		managers: [],
		stats: [],
		show_stat: false,
		
		status: false,
		timeout: null
	}
}

Vue.use(VueMask.VueMaskPlugin);
		
var App = new Vue({
	
	el: '#app',
	data: data(),
	
	watch: {
		phone: function (val) {
			
			if ( this.phone.replace(/[^\d;]/g, '').length > 7 ) {
				
				var headers = {
					'Content-Type': 'application/x-www-form-urlencoded',
					'Accept': 'application/json'
				};
				
				axios
					.post('https://apps.yug-avto.ru/API/foots/findclient/?token='+$cookies.get('user_token'), {phone: this.FormatPhoneIn(this.phone)}, {headers: headers})
					.then( function( response ) {
							
						App.client.result = response.data;
						App.client.status = ( response.data.status == 'success') ? true : false;
					});
			}
		},
	},
	
	mounted: function () {
		
		this.auth.token = $cookies.get('user_token') || false;
		
		if ( this.auth.token ) {
			axios
			  .get('https://apps.yug-avto.ru/API/foots/getuser/?token='+$cookies.get('user_token'))
			  .then( function(response) {
				  
				  App.auth.status = response.data.status;
				  App.auth.description = response.data.description;
				  
				  App.auth.dc = response.data.dcs[0]
				  App.auth.name = response.data.name;
				  
				  App.send.hostess_token = App.auth.token;
				  App.send.dc_name = App.auth.dc;
			  })
			  .catch(function (error) { console.log(error) });
		}
	},
	
	methods: {
		
		Auth( data ) {
			
			var headers = {
				'Content-Type': 'application/x-www-form-urlencoded',
				'Accept': 'application/json'
			};
			
			axios
				.post('https://apps.yug-avto.ru/API/Foots/auth/', data, {headers: headers})
				.then( function(response) {
					
					App.auth.status = response.data.status;
					App.auth.description = response.data.description;
					
					if ( response.data.public_key ) {
						
						App.auth.token = response.data.public_key;
						$cookies.set('user_token', response.data.public_key, 3600*12);
						
						App.auth.dc = response.data.dcs[0]
						App.auth.name = response.data.name;
						
						App.send.hostess_token = App.auth.token;
						App.send.dc_name = App.auth.dc;
					}
				})
				.catch( function(error) { console.log(error) });
			
		},
		UnAuth() {
			
			App.auth.status = null;
			App.auth.description = null,
			App.auth.token = false,
			App.auth.dc = null
			
			App.step = 1;
			
			$cookies.remove('user_token');
		},
		Reset() {
			
			App.phone = null;
			App.name = null;
			App.comment = null;
			App.client = {
				status: false,
				result: null,
				phone: ''
			}
			
			App.step = 1;
			App.send = {};
			App.managers = [];
			App.stats = [];
			App.status = false;
			App.timeout = null;
		},
		
		CheckArrangement( manager ) {
			
			App['manager'+manager.id] = !App['manager'+manager.id];
		},
		NextStep( data, step ) {
			
			for (var key in data) { App.send[key] = data[key] }
			
			switch ( Number(step) ) {
				
				case 1:
					App.step = step;
					break;
				
				case 2:
					axios
					  .get('https://apps.yug-avto.ru/API/foots/gettargets/?token='+$cookies.get('user_token'))
					  .then( function(response) {
						  
						  App.targets = response.data;
						  App.step = step;
					  })
					  .catch(function (error) { console.log(error) });
					break;
					
				case 3:
					axios
					  .get('https://apps.yug-avto.ru/API/foots/getmanagers/?token='+$cookies.get('user_token'))
					  .then( function(response) {
						  
						  Vue.set(App, 'managers', response.data);
						  App.send.arrangement = false;
						  App.step = step;
					  })
					  .catch(function (error) { console.log(error) });
					break;
					
				case 4:
					App.step = step;
					break;
					
				case 5:
					App.step = step;
					break;
					
				case 100:
					axios
					  .get('https://apps.yug-avto.ru/API/foots/getstat/?token='+$cookies.get('user_token'))
					  .then( function(response) {
						  
						  Vue.set(App, 'stats', response.data);
						  App.step = step;
					  })
					  .catch(function (error) { console.log(error) });
					break;
			}
		},
		SendStat() {
			
			if ( App.comment ) App.send.comment = App.comment;
			if ( App.client.status ) App.send.client_id = App.client.result.id;
			App.send.hostess_token = App.auth.token;
			App.send.dc_name = App.auth.dc;
			
			var headers = {
				'Content-Type': 'application/x-www-form-urlencoded',
				'Accept': 'application/json'
			};
			
			axios
				.post('https://apps.yug-avto.ru/API/foots/stat/?token='+$cookies.get('user_token'), App.send, {headers: headers})
				.then( function(response) {
					App.status = true;
					// App.timeout = setTimeout( function() { App.Reset() }, 5000);
				})
				.catch( function(error) { console.log(error) });
		},
		
		ShowStats() {
			
			axios
				.get('https://apps.yug-avto.ru/API/foots/getstat/?token='+$cookies.get('user_token'))
				.then( function(response) {
					
					Vue.set(App, 'stats', response.data);
					App.send.arrangement = false;
					App.step = step;
				})
				.catch(function (error) { console.log(error) });
		},
		SetWorklist( data ) {
			
			var headers = {
				'Content-Type': 'application/x-www-form-urlencoded',
				'Accept': 'application/json'
			};
			
			axios
				.post('https://apps.yug-avto.ru/API/foots/setworklist/?token='+$cookies.get('user_token'), data, {headers: headers})
				.then( function(response) {
					show_stat = false;
				})
				.catch( function(error) { console.log(error) });
		},
		
		FormatPhoneOut( phone ) {
			
			phone = String(phone).replace(/[^\d;]/g, '');
			return '+' + phone[0] + ' (' + phone[1] + phone[2] + phone[3] + ') ' + phone[4] + phone[5] + phone[6] + '-' + phone[7] + phone[8] + '-' + phone[9] + phone[10];
			
		},
		
		FormatPhoneIn( phone ) {
			
			var res = String(phone).replace(/[^\d;]/g, '');
			return res;
		}
	}
});
